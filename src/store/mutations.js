import * as types from './mutation-types'
const mutations = {
    [types.SET_USERINFO](state,userInfo) {
        state.userInfo = userInfo
    },
    [types.SET_COMPANYINFO](state,companyInfo) {
        state.companyInfo = companyInfo
    },
    [types.SET_ISSUE_ID](state,issue_id) {
        state.issue_id = issue_id
    },
    [types.SET_DESC_ID](state,desc_id) {
        state.desc_id = desc_id
    },
    [types.SET_PROPLEM_STEPS](state,proplem_steps) {
        state.proplem_steps = proplem_steps
    },
    [types.SET_LINGPEIJIANLIST](state,LingpeijianList) {
        state.LingpeijianList = LingpeijianList
    },
    [types.SET_LINGPEIJIANORDER](state,LingpeijianOrder) {
        state.LingpeijianOrder = LingpeijianOrder
    },
    [types.SET_WHICHLINGPEIJIAN](state,WhichLingpeijian) {
        state.WhichLingpeijian = WhichLingpeijian
    },
    [types.SET_DINGDANTYPE](state,DingdanType) {
        state.DingdanType = DingdanType
    },
    [types.SET_PROPLEMCOMPONENTSLIST](state,ProplemComponentsList) {
        state.ProplemComponentsList = ProplemComponentsList
    },
    [types.SET_CERAMPHOTO](state,CeramPhoto) {
        state.CeramPhoto = CeramPhoto
    },
    [types.SET_LINGPEIJIANDINGDANPHOTOSKEYS](state,LingpeijianDingdanPhotoKeys) {
        state.LingpeijianDingdanPhotoKeys = LingpeijianDingdanPhotoKeys
    },
    [types.SET_NEWSMSGS](state,NewsMsgs) {
        state.NewsMsgs = NewsMsgs
    },
}

export default mutations